@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listar Productos</title>
</head>
<body>
    <br><br><h1 align="center">Lista de los produtos en bodega</h1><br><br>
<a class="btn btn-primary" href="{{route('Products.Create')}}">Crear</a><br><br>

    @if (empty($Products))
    <div class="alert alert-warning">
        <h2>No hay datos</h2>
    </div>

    @else
    <div class="class-responsive">
        <table class="table table-striped" align="center">
            <thead class="table-primary">
                <tr align="center">
                    <th>Codigo producto</th>
                    <th>Nombre del producto</th>
                    <th>Descripción del producto</th>
                    <th>Precio del producto</th>
                    <th>Stock del producto</th>
                    <th>Opción</th>
                </tr>
            </thead>

            <tbody>
                @foreach($Products as $Product)
                <tr>
                <td>{{$Product->id}}</td>
                <td>{{$Product->name}}</td>
                <td>{{$Product->description}}</td>
                <td>{{$Product->price}}</td>
                <td>{{$Product->stock}}</td>
                <td><a class="btn btn-link" href="{{route('Products.Edit',['Products' => $Product->id])}}">Modificar</a>
                <form action="{{route('Products.Destroy',['Products'=> $Product->id])}}" class="d-inline" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-link">Eliminar Producto</button>
                </form>
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    @endif
</body>
</html>
@endsection
