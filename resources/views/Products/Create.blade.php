@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crear Producto</title>
</head>
<body>

    <h1 align="center">Crear Productos</h1>
    <form action="{{route('Products.Store')}}" method="POST">
        @csrf
        <div class="form-row">
            <label>Nombre del producto:</label>
        <input type="text" class="form-control" name="name" value="{{old('name')}}" required>
        </div>

        <div class="form-row">
            <label>Descripción:</label>
        <input type="text" class="form-control" name="description" value="{{old('description')}}" required>
        </div>

        <div class="form-row">
            <label>Precio:</label>
        <input type="number" class="form-control" name="price" min="1.0" step="0.1" value="{{old('price')}}" required>
        </div>

        <div class="form-row">
            <label>Stock:</label>
        <input type="number" class="form-control" name="stock" min="0" step="1" value="{{old('stock')}}" required>
        </div>

        <div class="form-row">
            <button type="submit" class="btn btn-primary btn-lg mt-3">Crear</button>
        </div>
    </form>

</body>
</html>
@endsection

