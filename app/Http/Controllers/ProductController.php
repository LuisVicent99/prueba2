<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    public function Index()
    {
        return view('Products.Index')->with(['Products'=>Product::all()]);
    }

    public function Create()
    {
        return view('Products.Create');
    }

    public function Store()
    {
        $Product = Product::create(request()->all());
        return redirect()->route('Products.Index');

    }

    public function Show($Products)
    {
        return "Crear Producto $Products";
    }

    public function Edit($Products)
    {
        return view('Products.Edit')->with(['Products'=> Product::findOrFail($Products)]);
    }

    public function Update($Products)
    {
        $Product = Product::findOrFail($Products);
        $Product->Update(request()->all());
        return redirect()->route('Products.Index');
    }

    public function Destroy($Products)
    {
        $Product = Product::findOrFail($Products);
        $Product->delete();
        return redirect()->route('Products.Index');
    }


}
