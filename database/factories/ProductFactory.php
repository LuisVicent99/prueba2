<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(8),
        'description' => $faker->sentence(40),
        'price' => $faker->randomFloat($maxDecimals =2, $min = 2, $max = 100),
        'stock' => $faker->numberBetween(1,10),
        //'status' => $faker->randomElement(['disponible','no disponible']),
    ];
});
