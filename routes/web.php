<?php

use App\Http\Controllers\ProductController;
use Illuminate\Routing\Route as RoutingRoute;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcomes');
});

Route::get('Product', 'ProductController@Index')->name('Products.Index');

Route::get('Product/Create', 'ProductController@Create')->name('Products.Create');

Route::post('Product','ProductController@Store')->name('Products.Store');

Route::get('Product/{Products}','ProductController@Show')->name('Products.Show');

Route::get('Product/{Products}/Edit', 'ProductController@Edit')->name('Products.Edit');

Route::match(['put', 'patch'], 'Product/{Products}','ProductController@Update')->name('Products.Update');

Route::delete('Product/{Products}','ProductController@Destroy')->name('Products.Destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('Products.Index');
